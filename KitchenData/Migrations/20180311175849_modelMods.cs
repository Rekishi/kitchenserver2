﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace KitchenData.Migrations
{
    public partial class modelMods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "ServerAddedDate",
                table: "ConsumableItems",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "KitchenId",
                table: "ConsumableItems",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "FromUserId",
                table: "ConsumableItems",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<DateTime>(
                name: "AllUsedDate",
                table: "ConsumableItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "GotItShoppingDate",
                table: "ConsumableItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ListAddedDate",
                table: "ConsumableItems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ManuallyDeleted",
                table: "ConsumableItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SyncedAllUsedOrDeleted",
                table: "ConsumableItems",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllUsedDate",
                table: "ConsumableItems");

            migrationBuilder.DropColumn(
                name: "GotItShoppingDate",
                table: "ConsumableItems");

            migrationBuilder.DropColumn(
                name: "ListAddedDate",
                table: "ConsumableItems");

            migrationBuilder.DropColumn(
                name: "ManuallyDeleted",
                table: "ConsumableItems");

            migrationBuilder.DropColumn(
                name: "SyncedAllUsedOrDeleted",
                table: "ConsumableItems");

            migrationBuilder.AlterColumn<string>(
                name: "ServerAddedDate",
                table: "ConsumableItems",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<Guid>(
                name: "KitchenId",
                table: "ConsumableItems",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "FromUserId",
                table: "ConsumableItems",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
