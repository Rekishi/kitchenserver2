﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace KitchenData.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConsumableItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Expiration = table.Column<DateTime>(nullable: true),
                    FromUserId = table.Column<Guid>(nullable: false),
                    GotItShopping = table.Column<bool>(nullable: false),
                    IdFromApp = table.Column<Guid>(nullable: false),
                    KitchenId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OnHand = table.Column<bool>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    ServerAddedDate = table.Column<string>(nullable: true),
                    UnitOfMeasure = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConsumableItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConsumableItems");
        }
    }
}
