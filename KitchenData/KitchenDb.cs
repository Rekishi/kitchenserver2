﻿using KitchenData.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace KitchenData
{
    public class KitchenDbContext : DbContext
    {
        // add-migration -c KitchenDbContext modelMods
        // update-database -c KitchenDbContext

        public KitchenDbContext(DbContextOptions<KitchenDbContext> options)
           : base(options)
        {
        }

        string ConnString { get; set; }

       

        public KitchenDbContext(string connString)
        {
            this.ConnString = connString;
        }


        public DbSet<KitchenConsumableItem> ConsumableItems { get; set; }
        public DbSet<KitchenConsumableItemLog> LogItems { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // temp
            var connString = "Server=tcp:brettserver.database.windows.net,1433;Initial Catalog=Main;Persist Security Info=False;User ID=brettadmin;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Password=This is the kitchen 1";

            optionsBuilder.UseSqlServer(connString);
        }

    }


    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<KitchenDbContext>
    {
        public KitchenDbContext CreateDbContext(string[] args)
        {

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<KitchenDbContext>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            builder.UseSqlServer(connectionString);

            return new KitchenDbContext(builder.Options);
        }
    }

}
