﻿using KitchenData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KitchenData
{
    public class ConsumableDataStore
    {
        private readonly KitchenDbContext context;

        public ConsumableDataStore(KitchenDbContext db)
        {
            context = db;
        }

        public async Task<List<KitchenConsumableItem>> SyncConsumables(IEnumerable<KitchenConsumableItem> items, Guid kitchenId)
        {
            if (items.Count() <= 0)
            {
                return null;
            }
            var sendBackItems = new List<KitchenConsumableItem>();


            // prep
            var itemsFromDbThatMatchKitchenId = context.ConsumableItems.Where(x => x.KitchenId == kitchenId).ToList();
            var dbItemIds = itemsFromDbThatMatchKitchenId.Select(x => x.IdFromApp);



            // add complete new items without checks
            var completelyNew = items.Where(x => !dbItemIds.Contains(x.IdFromApp));
            foreach(var item in completelyNew)
            {
                item.LastModDate = DateTime.Now;
            }
            context.ConsumableItems.AddRange(completelyNew);
            await context.SaveChangesAsync();


            // Save changes that might be applicable
            var dbSaveItems = new List<KitchenConsumableItem>();
            var dbExist = items.Where(x => dbItemIds.Contains(x.IdFromApp));
            foreach (var dbItem in dbExist)
            {
                var syncItem = items.Where(x => x.IdFromApp == dbItem.IdFromApp).First();

                if (dbItem.LastModDate > syncItem.LastModDate)
                {
                    CopyFromTo(dbItem, syncItem);
                    sendBackItems.Add(syncItem);
                } else
                {
                    CopyFromTo(syncItem,  dbItem);
                    dbSaveItems.Add(dbItem);
                }


            }
            await context.SaveChangesAsync();


            // send new items to app
            var itemsIds = items.Select(x => x.IdFromApp);
            var dbExistButNotApp = itemsFromDbThatMatchKitchenId.Where(x => !itemsIds.Contains(x.IdFromApp));
            foreach (var item in dbExistButNotApp)
            {
                sendBackItems.Add(item);
            }

            return sendBackItems;
        }

        private void CopyFromTo(KitchenConsumableItem copyFrom, KitchenConsumableItem copyTo)
        {
            copyFrom.FromUserId = copyTo.FromUserId;
            copyFrom.Name = copyTo.Name;
            copyFrom.Price = copyTo.Price;
            copyFrom.Quantity = copyTo.Quantity;
            copyFrom.Expiration = copyTo.Expiration;
            copyFrom.OnHand = copyTo.OnHand;
            copyFrom.GotItShopping = copyTo.GotItShopping;
            copyFrom.ListAddedDate = copyTo.ListAddedDate;
            copyFrom.GotItShoppingDate = copyTo.GotItShoppingDate;
            copyFrom.AllUsedDate = copyTo.AllUsedDate;
            copyFrom.ManuallyDeleted = copyTo.ManuallyDeleted;
            copyFrom.LastModDate = copyTo.LastModDate;
        }




    }
}
