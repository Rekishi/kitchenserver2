﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KitchenData.Models
{
    public class KitchenConsumableItem
    {
        public Guid Id { get; set; }
        public string FromUserId { get; set; }
        public Guid? KitchenId { get; set; } // Possibly use this to identify different kitchens
        public DateTime ServerAddedDate { get; set; }

        public Guid IdFromApp { get; set; }

        public string Name { get; set; }
        public double Price { get; set; }
        public int UnitOfMeasure { get; set; }
        public int Quantity { get; set; }
        public DateTime? Expiration { get; set; }
        public bool OnHand { get; set; }
        public bool GotItShopping { get; set; }

        public DateTime? ListAddedDate { get; set; }
        public DateTime? GotItShoppingDate { get; set; }
        public DateTime? AllUsedDate { get; set; }
        public bool ManuallyDeleted { get; set; }
        public bool SyncedAllUsedOrDeleted { get; set; }

        public DateTime LastModDate { get; set; }
    }
}