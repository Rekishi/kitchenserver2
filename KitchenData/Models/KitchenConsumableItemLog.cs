﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KitchenData.Models
{
    public class KitchenConsumableItemLog
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }
        public Guid ConsumableId { get; set; }
        public int ActionId { get; set; }
        public DateTime When { get; set; }
        public int Qty { get; set; }
    }
}
