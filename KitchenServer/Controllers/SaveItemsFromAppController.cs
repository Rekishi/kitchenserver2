﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KitchenData;
using KitchenData.Models;
using KitchenServer.Data;
using KitchenServer.Models;
using KitchenServer.Models.AppModels;
using KitchenServer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace KitchenServer.Controllers
{
    [Produces("application/json")]
    [Route("api/SaveItemsFromApp")]
    [Authorize]
    public class SaveItemsFromAppController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly KitchenDbContext kitchenDb;

        public SaveItemsFromAppController(UserManager<ApplicationUser> userManager, KitchenDbContext _kitchenDb)
        {
            _userManager = userManager;
            kitchenDb = _kitchenDb;
        }

        // GET: api/SaveItemsFromApp
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SaveItemsFromApp/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/SaveItemsFromApp
        [HttpPost]
        public async Task<IEnumerable<ConsumableDataItem>> Post([FromBody]IEnumerable<ConsumableDataItem> data)
        {
            var email = (HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity).Claims.Where(x => x.Type == "email").First().Value;
            var userId = await new UserWatcher(_userManager).Check(email);
            var serverTime = DateTime.Now;

            var dataItems = new List<KitchenConsumableItem>();

            foreach (var item in data)
            {
                dataItems.Add(item.ToKitchenConsumableDataItem(userId, serverTime));
            }

            
            // This needs changed!!!!!!
            var kitchenId = Guid.Empty;

            var ds = new ConsumableDataStore(kitchenDb);
            var itemsToSendBack = await ds.SyncConsumables(dataItems, kitchenId);

            var ret = new List<ConsumableDataItem>();
            foreach (var item in itemsToSendBack)
            {
                ret.Add(ConsumableDataItem.FromKitchenConsumableDataItem(item));
            }

            return ret;
        }

        // PUT: api/SaveItemsFromApp/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
