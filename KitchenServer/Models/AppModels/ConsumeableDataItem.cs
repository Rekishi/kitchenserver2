﻿using KitchenData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KitchenServer.Models.AppModels
{
    /// <summary>
    /// Taken Directly from the Kitchen app
    /// </summary>
    public class ConsumableDataItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public double Price { get; set; }
        public int UnitOfMeasure { get; set; }
        public int Quantity { get; set; }
        public DateTime? Expiration { get; set; }
        public bool OnHand { get; set; }
        public bool GotItShopping { get; set; }

        public Guid? KitchenId { get; set; }
        public DateTime? ListAddedDate { get; set; }
        public DateTime? GotItShoppingDate { get; set; }
        public DateTime? AllUsedDate { get; set; }
        public bool ManuallyDeleted { get; set; }
        public bool SyncedAllUsedOrDeleted { get; set; }

        public DateTime? LastModDate { get; set; }


        public KitchenConsumableItem ToKitchenConsumableDataItem(string userId, DateTime serverDateTime)
        {
            return new KitchenConsumableItem()
            {

                Id = Guid.Empty,
                FromUserId = userId,
                KitchenId = Guid.Empty,
                ServerAddedDate = serverDateTime,

                IdFromApp = this.Id,
                Name = this.Name,
                Price = this.Price,
                UnitOfMeasure = this.UnitOfMeasure,
                Quantity = this.Quantity,
                Expiration = this.Expiration,

                OnHand = this.OnHand,
                GotItShopping = this.GotItShopping,

                ListAddedDate = this.ListAddedDate,
                GotItShoppingDate = this.GotItShoppingDate,
                AllUsedDate = this.AllUsedDate,
                ManuallyDeleted = this.ManuallyDeleted,
                SyncedAllUsedOrDeleted = this.SyncedAllUsedOrDeleted,

                LastModDate = this.LastModDate ?? DateTime.Now
            };



        }

        public static ConsumableDataItem FromKitchenConsumableDataItem(KitchenConsumableItem item)
        {
            return new ConsumableDataItem()
            {

                Id = item.IdFromApp,
                AllUsedDate = item.AllUsedDate,
                Expiration = item.Expiration,
                GotItShopping = item.GotItShopping,
                GotItShoppingDate = item.GotItShoppingDate,
                KitchenId = item.KitchenId,
                LastModDate = item.LastModDate,
                ListAddedDate = item.ListAddedDate,
                ManuallyDeleted = item.ManuallyDeleted,
                Name = item.Name,
                OnHand = item.OnHand,
                Price = item.Price,
                Quantity = item.Quantity,
                SyncedAllUsedOrDeleted = item.SyncedAllUsedOrDeleted,
                UnitOfMeasure = item.UnitOfMeasure

            };



        }
    }


}
