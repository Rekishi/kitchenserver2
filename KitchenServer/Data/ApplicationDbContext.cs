﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using KitchenServer.Models;

namespace KitchenServer.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        //public class ConsumableItem
        //{
        //    public Guid Id { get; set; }
        //    public Guid FromUserId { get; set; }

        //    public Guid IdFromApp { get; set; }

        //    public string Name { get; set; }
        //    public double Price { get; set; }
        //    public int UnitOfMeasure { get; set; }
        //    public int Quantity { get; set; }
        //    public DateTime? Expiration { get; set; }
        //    public bool OnHand { get; set; }
        //    public bool GotItShopping { get; set; }
        //}

    }
}
