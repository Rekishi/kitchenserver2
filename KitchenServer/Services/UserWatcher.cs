﻿using KitchenServer.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace KitchenServer.Services
{
    public class UserWatcher
    {
        private UserManager<ApplicationUser> userManager;

        public UserWatcher(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
        }

        public async Task<string> Check(string email)
        {
            var user = await userManager.FindByEmailAsync(email);

            if (user == null)
            {
                var ir = await userManager.CreateAsync(new ApplicationUser()
                {
                    Email = email
                });

                if (ir.Succeeded)
                {
                    user = await userManager.FindByEmailAsync(email);
                }
            }

            return user.Id;
        }
    }
}
