﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using KitchenServer.Data;
using KitchenServer.Models;
using KitchenServer.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Google.Apis.Auth;

namespace KitchenServer
{
    public class Startup
    {

        public string googleSecret = "";

        public Startup(IConfiguration configuration)
        {
            //Configuration = configuration;

            var builder = new ConfigurationBuilder();
            builder.AddUserSecrets<Startup>();
            Configuration = builder.Build();

            foreach (var item in configuration.AsEnumerable())
            {
                Configuration[item.Key] = item.Value;
            }

        }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var dbpassword = Configuration["brettdbpassword"];


            string baseConnString = Configuration.GetConnectionString("DefaultConnection");
            string connString = baseConnString + $";Password={dbpassword}";

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connString));

            services.AddDbContext<KitchenData.KitchenDbContext>(options =>
                options.UseSqlServer(connString));
            

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();

            googleSecret = Configuration["GooglePlusSecret"];

            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.ClientSecret = Configuration["GooglePlusSecret"];
                googleOptions.ClientId = Configuration["GoogelPlusAppId"];
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })
             .AddJwtBearer(o =>
             {
                 o.SecurityTokenValidators.Clear();
                 o.SecurityTokenValidators.Add(new GoogleTokenValidator());
             });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }


        public class GoogleTokenValidator : ISecurityTokenValidator
        {
            private readonly JwtSecurityTokenHandler _tokenHandler;

            public GoogleTokenValidator()
            {
                _tokenHandler = new JwtSecurityTokenHandler();
            }

            public bool CanValidateToken => true;

            public int MaximumTokenSizeInBytes { get; set; } = TokenValidationParameters.DefaultMaximumTokenSizeInBytes;

            public bool CanReadToken(string securityToken)
            {
                return _tokenHandler.CanReadToken(securityToken);
            }

            public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
            {
                validatedToken = null;
                var payload = GoogleJsonWebSignature.ValidateAsync(securityToken, new GoogleJsonWebSignature.ValidationSettings()).Result; // here is where I delegate to Google to validate

                var claims = new List<Claim>
                {
                    //new Claim(ClaimTypes.NameIdentifier, payload.Name),
                    //new Claim(ClaimTypes.Name, payload.Name),
                    //new Claim(JwtRegisteredClaimNames.FamilyName, payload.FamilyName),
                    //new Claim(JwtRegisteredClaimNames.GivenName, payload.GivenName),
                    new Claim(JwtRegisteredClaimNames.Email, payload.Email),
                    new Claim(JwtRegisteredClaimNames.Sub, payload.Subject),
                    new Claim(JwtRegisteredClaimNames.Iss, payload.Issuer),
                };

                try
                {
                    var principle = new ClaimsPrincipal();
                    principle.AddIdentity(new ClaimsIdentity(claims, "password"));



                    return principle;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;

                }
            }
        }
    }
}
